# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com), and this
project adheres to [Semantic Versioning](https://semver.org).

## Types of changes
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [8.2.1] - 2023-02-15

- Removed underflow message in FALL3D task (crop of small values in netCDF)

### Added
- README.md file
- CHANGELOG.md file

### Changed
- Gold badge has been awarded (EOSC-Synergy)

### Removed
- NOTES file

## [8.2.0] - 2022-07-29

### Added
- Concentration at surface output
- Wind rotation option
- RAMS diffusion model

### Changed
- File extensions changed from f90 to F90 (except mod_Configure.f90.in)
- Ported to accelerators using OpenACC (on branch master_gpu)

### Fixed
- Gravity current bug corrected
- Correction of minor bugs

## [8.1.1] - 2021-07-23

### Added
- Tasks PosVal added for validation of results

### Fixed
- Correction of minor bugs

## [8.1.0] - 2020-04-22

### Added
- Ensemble simulation
- Tasks PosEns added to postprocess ensemble runs (probabilistic forecasts)

### Changed
- Wet deposit enabled for radionuclide decay

### Fixed
- Correction in the deposit for wet deposition (it now includes all parts; also in the tps files)

## [8.0.1] - 2020-02-13

### Added
- Complete version; all functionalities in Folch et al. (2019) active
- Input file blocks completed, backwards compatibility will be kept until version 9.x

### Changed
- Output netCDF files follow Climate and Forecast (CF) Metadata Conventions

### Fixed
- Bug in data assimilation profiles
- Bug in sources and tracking points when the domain crosses the meridian day
- Bug in SetDbs for when using single precision

### Removed
- Global attribute `run_start_time`  

## [8.0.0] - 2019-12-20

### Added
- First public release of FALL3D v8.0.0
- Preliminary and incomplete version, some functionalities still not active
- Some changes in the blocks of the input file name.inp will exist in the final version release

