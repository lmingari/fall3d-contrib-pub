# FALL3D Utilities

A set of utilities are provided in this folder to obtain and pre-process meteorological data.
In order to specify the required dataset, you can use command-line arguments to pass the 
input arguments to the scripts. Alternatively, an input file can be also used to define 
the configuration parameters (see the `config.inp` for an example). The `config.inp` file 
allows to define multiple configurations using specific blocks (e.g., to set different domain 
configurations). For example, the following command

```
> ./gfs.py --input config.inp --block etna
```
will download GFS data using the configuration defined in the `etna` block of the `config.inp` 
input file.

For more information, use:

```
> ./gfs.py --help
```

## NCEP operational data

Two scripts are available to download NCEP operational data in grib format:

* `gfs.py`: Download GFS data (requires fall3dutil python package)
* `gefs.py`: Download GEFS data (requires fall3dutil python package)

Grib files have to be converted to netCDF to be ingested by FALL3D. 
To this purpose a script is available:

* `grib2nc.sh`: Concatenate a list of grib files and convert them to netCDF (requires wgrib2 tool)

## ERA5 reanalysis data

In order to download ERA5 reanalysis date, you have to install a CDS API key. 
For more information, visit this [link](https://cds.climate.copernicus.eu/api-how-to).

Three scripts are available to download ERA5 data in netCDF format:

* `era5_sfc.py`: Download ERA5 data (single level and surface)
* `era5_pl.py`: Download ERA5 data (pressure levels)
* `era5_ml.py`: Download ERA5 data (model levels)

In all cases, the fall3dutil python package is required.

You have two options:

### ERA5 - pressure levels
Use the scripts `era5_sfc.py` and `era5_pl.py` to obtain two netCDF files, e.g., `sfc.nc` and `pl.nc`, respectively. Then, concatenate them to create a full dataset file `merged.nc`. For example, using the `cdo` utility:

```
cdo merge pl.nc sfc.nc merged.nc
```
The order is important here (use the pressure level file as the first argument)!

The `merged.nc` file can be read by FALL3D.

### ERA5 - model levels
Use the scripts `era5_sfc.py` and `era5_ml.py` to obtain two netCDF files, e.g., `sfc.nc` and `ml.nc`, respectively. Then, concatenate them to create a full dataset file `merged.nc`. For example, using the `cdo` utility:

```
cdo merge ml.nc sfc.nc merged.nc
```
The order is important here (use the model level file as the first argument)!

The `merged.nc` file can be read by FALL3D.
