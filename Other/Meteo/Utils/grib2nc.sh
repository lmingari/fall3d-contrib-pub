#!/bin/bash

# Description:
#   Convert a set of GFS/GEFS grib files 
#   into a single netcdf file
#   which can be read by FALL3D.
#
# User intervention:
#   Modify only the header section!
#   You have to define the filename format 
#   for the input grib files. For example, 
#   if the input filenames are:
#   gfs.t00z.pgrb2.0p25.f000
#   gfs.t00z.pgrb2.0p25.f001 ...
#   edit the fname variable in the GRIBFILE 
#   function as:
#   fname="gfs.t${CYCLE}z.pgrb2.0p25.f${HOUR}"
#
# Parameters:
# - OUTPUTFILE: 
#   filename for the netCDF output file
# - TABLEFILE: 
#   auxiliary file defining pressure levels.
#   You have to define "TABLEFILE" depending on 
#   the GFS resolution (the list of pressure 
#   levels depends on the resolution)
# - TMIN/TMAX:
#   refers to min/max forecast hours.
# - CYCLE:
#   is the cycle time (0,6,12,18)
# - DATE:
#   date in format YYYYMMDD
# - GRIBPATH:
#   path with the grib files
# - GRIBFILE:
#   function returning the grib filename

########## Edit header ########## 
WGRIBEXE=wgrib2
OUTPUTFILE=output.nc
TABLEFILE=grib_tables/gfs_1p00.levels
TMIN=0
TMAX=12
STEP=6
CYCLE=12
DATE=20230404
GRIBPATH=~/fall3d/fall3dutil/tests
GRIBFILE (){
    fname=${GRIBPATH}/gfs.t${CYCLE}z.pgrb2.1p00.f${HOUR}
#    fname=${GRIBPATH}/gfs.t${CYCLE}z.pgrb2.0p25.f${HOUR}
#    fname=${GRIBPATH}/gfs.t${CYCLE}z.pgrb2full.0p50.f${HOUR}
#    fname=${GRIBPATH}/gep04.t${CYCLE}z.pgrb2a.0p50.f${HOUR}
    echo ${fname}
}
################################# 

variables="HGT|TMP|RH|UGRD|VGRD|VVEL|PRES|PRATE|LAND|HPBL"

if [ -f $OUTPUTFILE ] 
then
    echo "File ${OUTPUTFILE} exists. It will be overwritten!"
    read -p "Do you want to proceed? (y/n) " yn

    case $yn in
            [yY] ) 
                echo "Removing ${OUTPUTFILE}..."
                rm ${OUTPUTFILE}
                ;;
            * ) 
                echo exiting...
                exit 1
                ;;
    esac
fi

CYCLE=$(printf %02d $CYCLE)
for i in $(seq ${TMIN} ${STEP} ${TMAX})
do 
    HOUR=$(printf %03d $i)
    echo "Processing $(GRIBFILE)..."
    ${WGRIBEXE} "$(GRIBFILE)" \
        -match ":(${variables}):" \
        -match ":(([0-9]*[.])?[0-9]+ mb|surface|2 m above ground|10 m above ground):" \
        -nc_table "${TABLEFILE}" \
        -append \
        -nc3 \
        -netcdf \
        "${OUTPUTFILE}" > wgrib.log
done
